package me.justicepro.starcore;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;

import me.justicepro.starcore.Chat.ModuleChat;
import me.justicepro.starcore.Rank.ModuleRank;

public class StarCore extends JavaPlugin
{

	public static HashMap<String, Module> modules = new HashMap<>();
	public static StarCore instance;
	public static File playerDir;

	public static void registerCommand(MCommand command, String fallback)
	{
		SimpleCommandMap commandMap = ((CraftServer)Bukkit.getServer()).getCommandMap();
		commandMap.register(fallback, command.getCommand());
	}

	@Override
	public void onEnable()
	{
		instance = this;
		playerDir = new File(getDataFolder(), "players");
		Config.init();

		registerModules();

		for (Module module : modules.values())
		{
			module.onEnable();
		}
	}

	@Override
	public void onDisable()
	{
		for (Module module : modules.values())
		{
			module.onDisable();
		}
	}

	public void registerModules()
	{
		registerModule(new ModuleChat());
		registerModule(new ModuleRank());
	}

	public void registerModule(Module module)
	{
		modules.put(module.getName(), module);
		Bukkit.getPluginManager().registerEvents(module, this);
	}

}