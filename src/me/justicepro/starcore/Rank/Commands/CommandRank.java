package me.justicepro.starcore.Rank.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.justicepro.starcore.MCommand;
import me.justicepro.starcore.PermssionableOP;
import me.justicepro.starcore.StarCore;
import me.justicepro.starcore.Rank.ModuleRank;

public class CommandRank extends MCommand
{

	public CommandRank()
	{
		super("rank", new PermssionableOP());
	}
	
	@Override
	public void onExecutePlayer(Player player, String alias, String[] args)
	{
		execute(player, alias, args);
	}
	
	@Override
	public void onExecute(CommandSender sender, String alias, String[] args)
	{
		execute(sender, alias, args);
	}
	
	public void execute(CommandSender sender, String alias, String[] args)
	{
		ModuleRank m = (ModuleRank) StarCore.modules.get("Rank");
		
		if (args.length >= 1)
		{
			
			if (args[0].equals("list"))
			{
				for (String rank : ModuleRank.ranks.keySet())
				{
					m.sendMessage(rank, sender);
				}
				return;
			}
			
			if (args[0].equals("set"))
			{
				if (args.length == 3)
				{
					if (ModuleRank.ranks.containsKey(args[2]))
					{
						m.setRank(Bukkit.getOfflinePlayer(args[1]), ModuleRank.ranks.get(args[2]));
						m.sendMessage(args[2] + "'s rank was changed to \"" + args[2] + "\".", sender);
					}else
					{
						m.sendMessage("Rank \"" + args[2] + "\" not found.", sender);
					}
				}else
				{
					m.sendMessage("Usage: /rank set <player> <rank>", sender);
				}
				
				return;
			}
			
			if (args[0].equals("get"))
			{
				if (args.length == 2)
				{
					m.sendMessage("The player \"" + args[1] + "\" has the rank \"" + m.getRank(Bukkit.getOfflinePlayer(args[1])) + "\".", sender);
				}else
				{
					m.sendMessage("Usage: /rank get <player>", sender);
				}
				
				return;
			}
			
		}
		
		m.sendMessage("/rank set <player> <rank>", sender);
		m.sendMessage("/rank get <player>", sender);
		m.sendMessage("/rank list", sender);
		
	}
	
}