package me.justicepro.starcore.Rank;

import java.io.File;
import java.util.HashMap;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

import me.justicepro.starcore.Module;
import me.justicepro.starcore.StarCore;
import me.justicepro.starcore.Rank.Commands.CommandRank;
import me.justicepro.starutils.Rank;
import me.justicepro.starutils.Data.Data;
import me.justicepro.starutils.Data.PlayerData;
import me.justicepro.starutils.Data.RankData;

public class ModuleRank extends Module
{
	
	public static HashMap<String, Rank> ranks = new HashMap<>();
	
	public ModuleRank()
	{
		super("Rank");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEnable()
	{
		File file = new File(StarCore.instance.getDataFolder(), "Ranks.star");
		try
		{
			if (!file.exists())
			{
				ranks.put("Default", new DefaultRank());
				RankData data = new RankData();
				data.ranks = ranks;
				Data.writeObjectToFile(file, data);
			}else
			{
				ranks = (HashMap<String, Rank>) Data.readObjectFromFile(file);
			}
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		registerCommand(new CommandRank());
	}

	public static void setName(Player player, String rank)
	{
		String name = ranks.get(rank).prefix + " " + player.getName();
		player.setDisplayName(name);
		player.setPlayerListName(name);
	}
	
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		setName(event.getPlayer(), getRank(event.getPlayer()));
	}

	public String getRank(OfflinePlayer player)
	{
		PlayerData d = PlayerData.readPlayerData(player.getUniqueId(), StarCore.playerDir);
		HashMap<String, Object> data = d.map;
		
		if (!data.containsKey("rank"))
		{
			setRank(player, ranks.get("Default"));
		}
		
		return (String) data.get("rank");
	}
	
	public void setRank(OfflinePlayer player, Rank rank)
	{
		
		if (player.isOnline())
		{
			Player player2 = player.getPlayer();
			setName(player2, rank.name);
			sendMessage("Your rank was changed to \"" + rank + "\".", player2);
		}
	}
	
}