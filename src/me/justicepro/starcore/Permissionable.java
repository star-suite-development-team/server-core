package me.justicepro.starcore;

import org.bukkit.entity.Player;

public interface Permissionable
{
	
	public boolean hasPermission(Player player);
	
}