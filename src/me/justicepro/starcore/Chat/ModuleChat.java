package me.justicepro.starcore.Chat;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import me.justicepro.starcore.Module;

public class ModuleChat extends Module
{
	
	public ModuleChat()
	{
		super("Chat");
	}
	
	public static void sendMessage(String prefix, String message, CommandSender sender)
	{
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&6" + prefix + "&e] &6" + message));
	}
	
}