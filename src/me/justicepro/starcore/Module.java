package me.justicepro.starcore;

import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

import me.justicepro.starcore.Chat.ModuleChat;

public class Module implements Listener
{
	
	private String name;
	
	public Module(String name)
	{
		this.name = name;
	}
	
	public void onEnable() {}
	public void onDisable() {}
	
	/**
	 * Register an {@link MCommand}.
	 * @param command
	 */
	public void registerCommand(MCommand command)
	{
		StarCore.registerCommand(command, name);
	}
	
	/**
	 * @return The name of the module.
	 */
	public String getName()
	{
		return name;
	}
	
	public void sendMessage(String message, CommandSender sender)
	{
		ModuleChat.sendMessage(name, message, sender);
	}
	
}