package me.justicepro.starcore;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MCommand
{
	
	private String name;
	private Command command;
	
	public MCommand(String name)
	{
		this.name = name;
		this.command = new Command(name) {
			
			@Override
			public boolean execute(CommandSender sender, String alias, String[] args)
			{
				if (sender instanceof Player)
				{
					Player player = (Player) sender;
					onExecutePlayer(player, alias, args);
				}else
				{
					onExecute(sender, alias, args);
				}
				return false;
			}
		};
	}
	
	public MCommand(String name, Permissionable permissionable)
	{
		this.name = name;
		this.command = new Command(name) {
			
			@Override
			public boolean execute(CommandSender sender, String alias, String[] args)
			{
				if (sender instanceof Player)
				{
					Player player = (Player) sender;
					
					if (permissionable.hasPermission(player))
					{
						onExecutePlayer(player, alias, args);
					}else
					{
						onPermissionFail(player, alias, args);
					}
					
				}else
				{
					onExecute(sender, alias, args);
				}
				return false;
			}
		};
	}
	
	public void onPermissionFail(Player player, String alias, String[] args)
	{
		player.sendMessage(ChatColor.RED + "You don't have permission to do that.");
	}
	
	public void onExecutePlayer(Player player, String alias, String[] args) {}
	public void onExecute(CommandSender sender, String alias, String[] args) {}
	
	public Command getCommand()
	{
		return command;
	}
	
	public String getName()
	{
		return name;
	}
	
}